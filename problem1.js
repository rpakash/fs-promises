const fs = require("fs");
const path = require("path");

function createDirectory() {
  const dirName = "randomDir";
  const dirPath = path.join(__dirname, dirName);

  return new Promise((resolve, reject) => {
    fs.mkdir(dirPath, (err) => {
      if (err) {
        if (err.code !== "EEXIST") {
          console.Error("Error creating directory");
          reject(err);
        } else {
          console.log("Created directory");
          resolve(dirPath);
        }
      } else {
        console.log("Created directory");
        resolve(dirPath);
      }
    });
  });
}

function writeFiles(dirPath) {
  const fileNames = Array(10)
    .fill(0)
    .map((_, index) => {
      return `${index}.json`;
    });

  const writeAllFilesPromise = fileNames.map((fileName) => {
    return new Promise((resolve, reject) => {
      const data = { randomNumber: Math.random() * 10000 };

      fs.writeFile(path.join(dirPath, fileName), JSON.stringify(data), (err) => {
        if (err) {
          console.log("Failed to write file " + fileName);
          reject(err);
        } else {
          //If the file is written successfully then send the file path
          console.log(fileName + " file written succesfully");
          resolve(path.join(dirPath, fileName));
        }
      });
    });
  });

  return Promise.allSettled(writeAllFilesPromise);
}

function deleteFiles(fileNamesWithPath) {
  //Get only the files names that are written successfully or fullfilled
  let resolvedFiles = fileNamesWithPath
    .filter((file) => {
      return file.status === "fulfilled";
    })
    .map((file) => {
      return file.value;
    });

  let deleteFilePromises = resolvedFiles.map((file) => {
    return new Promise((resolve, reject) => {
      fs.unlink(file, (err) => {
        if (err) {
          console.log("Failed to delete file " + file);
          reject(err);
        } else {
          console.log(file + " file deleted successfully");
          resolve();
        }
      });
    });
  });

  return Promise.allSettled(deleteFilePromises);
}

function problem1() {
  createDirectory()
    .then((path) => {
      return writeFiles(path);
    })
    .then((data) => {
      console.log("File writing operation completed");

      return deleteFiles(data);
    })
    .then(() => {
      console.log("File deletion operation completed");
    })
    .catch((err) => {
      console.log("Error Occured");
      console.log(err);
    });
}

module.exports = problem1;
