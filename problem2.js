const fs = require("fs");
const path = require("path");

function writeFile(path, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (err) => {
      if (err) {
        console.log("Failed to write file " + path);
        reject(err);
      } else {
        console.log(`${path} file written`);
        resolve();
      }
    });
  });
}

function appendToFile(path, data) {
  return new Promise((resolve, reject) => {
    fs.appendFile(path, data, (err) => {
      if (err) {
        console.log("Failed to append data to file " + path);
        reject(err);
      } else {
        console.log(`${path} file modified`);

        resolve();
      }
    });
  });
}

function readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log("Failed to read file " + path);
        reject(err);
      } else {
        console.log(`${path} file read successfull`);

        resolve(data.toString());
      }
    });
  });
}

function problem2() {
  readFile(path.join(__dirname, "lipsum.txt"))
    .then((data) => {
      return writeFile(path.join(__dirname, "upper.txt"), data.toLocaleUpperCase());
    })
    .then(() => {
      return writeFile(path.join(__dirname, "fileNames.txt"), "upper.txt");
    })
    .then(() => {
      return readFile(path.join(__dirname, "upper.txt"));
    })
    .then((data) => {
      let lowercaseWithSplit = data.toLocaleLowerCase().split(".");
      return writeFile(path.join(__dirname, "lower.txt"), lowercaseWithSplit.join("\n"));
    })
    .then(() => {
      return appendToFile(path.join(__dirname, "fileNames.txt"), "\nlower.txt");
    })
    .then(() => {
      return readFile(path.join(__dirname, "lower.txt"));
    })
    .then((data) => {
      return new Promise((resolve, reject) => {
        readFile(path.join(__dirname, "upper.txt"))
          .then((upperData) => {
            resolve([...data.split(" "), ...upperData.split(" ")]);
          })
          .catch((err) => {
            reject(err);
          });
      });
    })
    .then((data) => {
      data.sort((wordA, wordB) => {
        return wordA.localeCompare(wordB);
      });

      return writeFile(path.join(__dirname, "sort.txt"), data.join("\n"));
    })
    .then(() => {
      return appendToFile(path.join(__dirname, "fileNames.txt"), "\nsort.txt");
    })
    .then(() => {
      return readFile(path.join(__dirname, "fileNames.txt"));
    })
    .then((fileNamesData) => {
      let fileNamesArray = fileNamesData.toString().split("\n");

      let deleteFilePromises = fileNamesArray.map((file) => {
        return new Promise((resolve, reject) => {
          fs.unlink(path.join(__dirname, file), (err) => {
            if (err) {
              console.log(`Failed to delete file ${file}`);
              reject(err);
            } else {
              console.log(`${file} file deleted`);

              resolve();
            }
          });
        });
      });

      return Promise.allSettled(deleteFilePromises);
    })
    .then(() => {
      console.log("File delete operation completed");
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports = problem2;
